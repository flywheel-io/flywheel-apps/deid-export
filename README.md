# Anonymized/De-identified Export

## Overview

### Summary

Profile-based anonymization and export of files within a container
(project/subject/session). Files within the source project will be
anonymized according to a de-id YAML profile and exported to a specified project.

Currently supported files are:

* Dicom
* JPG
* PNG
* TIFF
* XML
* JSON
* Text file defining key/value pair (e.g. MHD)
* CSV
* TSV

Currently supported field transformations are:

* ``remove``: Removes the field from the metadata.
* ``replace-with``: Replaces the contents of the field with the value provided.
* ``increment-date``: Offsets the date by the number of days.
* ``increment-datetime``: Offsets the datetime by the number of days.
* ``hash``: Replace the contents of the field with a one-way cryptographic hash.
* ``hashuid``: Replaces a UID field with a hashed version of that field.
* ``jitter``: Shifts value by a random number.
* ``encrypt`` (non-DICOM): Encrypts the field in place with AES-EAX encryption
* ``encrypt`` (DICOM): Removes the field from the DICOM and stores the original value
in EncryptedAttributesSequence with CMS encryption
* ``decrypt`` (non-DICOM): Decrypts the field in place with AES-EAX decryption
* ``decrypt`` (DICOM): Replace the contents of the field with the value stored in
EncryptedAttributesSequence with CMS decryption
* ``regex-sub``: Replace the contents of the field with a value built from other fields
  and/or group extracted from the field value.
* ``keep``: Do nothing.

Additionally, for DICOM, pixel data masking is supported based on pre-defined
pixel coordinates ([doc](https://flywheel-io.gitlab.io/public/migration-toolkit/pages/pixels.html)).

The YAML profile extends the
[flywheel-migration-toolkit](https://gitlab.com/flywheel-io/public/migration-toolkit)
de-id profile to flywheel metadata container. Documentation on how to write YAML
configuration for the different supported files can be found in the flywheel-migration
[doc](https://flywheel-io.gitlab.io/public/migration-toolkit/).

_NOTE:_ A requirement for metadata generation is that the destination
project has a gear rule configured for metadata indexing (e.g. dicom-mr-classifier), as
the gear
itself does not propagate/modify DICOM metadata.

### License

MIT

### Classification

analysis

_Gear Level:_

* [x] Project
* [x] Subject
* [x] Session
* [ ] Acquisition
* [ ] Analysis

----

[[_TOC_]]

----

### Inputs

* _deid_profile_
  * __Name__: deid_profile
  * __Type__: file
  * __Optional__: false
  * __Description__: A Flywheel de-identification profile specifying the
      de-identification actions to perform

* _subject_csv_
  * __Name__: subject_csv
  * __Type__: file
  * __Optional__: true
  * __Description__: A CSV file that contains mapping values to apply for subjects
      during de-identification.

#### deid_profile (required)

This is a YAML file that describes the protocol for de-identifying
input_file. This file covers all the same functionality of Flywheel
CLI de-identification. A simple example deid_profile.yaml looks like this:

``` yaml
# Configuration for DICOM de-identification
dicom:
  # What date offset to use, in number of days
  date-increment: -17

  # Set patient age from date of birth
  patient-age-from-birthdate: true
  # Set patient age units as Years
  patient-age-units: Y
  # Remove private tags
  remove-private-tags: true

  # filenames block to manipulate output filename based on input filename
  filenames:
      # input regular expression that match source filename
    - input-regex: '.*'
      # formatter of the output filename
      output: '{SOPInstanceUID}.dcm'

  fields:
    # Remove a dicom field value (e.g. remove “StationName”)
    - name: StationName
      remove: true

    # Increment a date field by -17 days
    - name: StudyDate
      increment-date: true

    # Increment a datetime field by -17 days
    - name: AcquisitionDateTime
      increment-datetime: true

    # One-Way hash a dicom field to a unique string
    - name: AccessionNumber
      hash: true

    # One-Way hash the ConcatenationUID,
    # keeping the prefix (4 nodes) and suffix (2 nodes)
    - name: ConcatenationUID
      hashuid: true

# Zip profile to handle e.g. .dcm.zip archive. All member file will be de-id accordly
to that same profile. 
zip:
  fields:
  - name: comment
    replace-with: FLYWHEEL
  filenames:
  - input-regex: (?P<used>.*).dicom.zip$
    output: '{used}.dcm.zip'
  hash-subdirectories: true
  validate-zip-members: true

# The flywheel configuration to handle flywheel metadata de-id.
flywheel:
  # subject container
  subject:
    # If set to true, export all source container metdata to destination container.
    all: true

  # session container
  session:
    # If set to false, only export to destination container the metadata defined
    # in the fields key.
    all: false
    date-increment: -17
    fields:
      - name: operator
        replace-with: REDACTED
      - name: info.sessiondate
        increment-date: true
      - name: tags
        replace-with: 
          - deid-exported

  acquisition:
    all: true

  file:
    all: true
    # If set to true, export the file info header to the destination container.
    # If set to false or missing, the file info header will be removed from the 
    # destination container.
    include-info-header: true
```

##### flywheel metadata de-id

The `flywheel` key can be used to transform Flywheel metadata and is referred
as the de-id flywheel profile. This profile defines the transformations to be
applied to each container to be exported.

Each container settings is based on the
[JSON file profile](https://flywheel-io.gitlab.io/public/migration-toolkit/pages/file_profiles.html#json-specific-file-settings)
and supports all the same functionality.

The fields that can be modified are listed below by container type:

```python
META_ALLOW_LIST_DICT = {
'file': {'classification', 'info', 'modality', 'type'},
'acquisition': ('timestamp', 'timezone', 'uid', 'info', 'label', 'tags'),
'session': ('age', 'operator', 'timestamp', 'timezone', 'uid', 'weight', 'info',
            'label', 'tags'),
'subject': ('firstname', 'lastname', 'sex', 'cohort', 'ethnicity', 'race',
            'species', 'strain', 'info', 'type', 'label', 'tags'),
'project': ('info', 'description', 'tags')
}
```

In addition, it comes with the following features:

1. `file.tags` are copied over to the destination container.
2. By default, `file.info.header` is removed from the destination container. If you
   want to keep it, set `include-info-header` to true.
3. It supports transforming fields on project, subject, session, acquisition and file
   containers
4. `all` settings (boolean): If set to true, it will export all source metadata to the
   destination container (as listed in the `META_ALLOW_LIST_DICT` dictionary above)
   applying the transformations defined in `fields`.
5. Parent container metadata can be referenced from any container block.

_NOTE:_  Given `file.info.header` is getting removed by default, it is recommended to
repopulate the `file.info.header` on the destination project by running the
file-metadata-importer gear on the destination project.

#### Subject-level customization

##### subject_csv (optional)

The subject_csv facilitates subject-specific configuration of
de-identification profiles. This is a csv file that contains the column
`subject.label` with unique values corresponding to the `subject.label`
values in the project to be exported. If a subject in the project to be
exported is not listed in `subject.label` in the provided subject_csv
this subject will not be exported.

##### subject metadata (optional)

Another option for subject-level customization is to leverage the metadata stored in
the Subject container. Subject metadata are collected by the gear at runtime and
utilized to fill deid profile variables that begin with `subject.`, for example
`subject.info.date_increment` could be set as metadata and notated in the deid profile
as `"{{ subject.info.date_increment }}"`.

##### Subject-level customization with subject_csv, subject.info and deid_profile

Requirements:

* To update subject fields, the fields must both be represented in the
  deid_profile as jinja variable (i.e `"{{ var_name }}"`) and either the
  subject_csv input file as column header or `subject.info` metadata.
* If a field is represented in both the deid_profile and the
  subject_csv, the value in the deid_profile will be replaced with the
  value listed in the corresponding column of the subject_csv for each
  subject that has a label listed in the `subject.label` column.
* Fields represented in the deid_profile but not in the subject_csv will
  be the same for all subjects.

Let's walk through an example of utilizing subject_csv, subject metadata, and
deid_profile to illustrate.

The following table represents subject_csv (../tests/data/example-csv-mapping.csv):

| subject.label | DATE_INCREMENT | SUBJECT_ID  | PATIENT_BD_BOOL |
|---------------|----------------|-------------|-----------------|
| 001           | -15            | Patient_IDA | false           |
| 002           | -20            | Patient_IDB | true            |
| 003           | -30            | Patient_IDC | true            |

For this example, we will assume that the Flywheel metadata `subject.info.name`
is set to `"Doe^John"`.

The deid_profile:

``` yaml
dicom:
  # date-increment can be any integer value since dicom.date-increment is defined in
  # example-csv-mapping.csv
  date-increment: "{{ DATE_INCREMENT }}"
  # since example-csv-mapping.csv doesn't define dicom.remove-private-tags,
  # all subjects will have private tags removed
  remove-private-tags: true
  fields:
    - name: PatientBirthDate
      # remove can be any boolean since dicom.fields.PatientBirthDate.remove is defined
      # in example-csv-mapping.csv
      remove: "{{ PATIENT_BD_BOOL }}"
    - name: PatientID
      # replace-with can be any string value since dicom.fields.PatientID.replace-with
      # is defined in example-csv-mapping.csv
      replace-with: "{{ SUBJECT_ID }}"
    - name: PatientName
      # Here, instead of using a value from example-csv-mapping.csv, we'll pull it from
      # Flywheel subject metadata. The jinja variable formatting is the same, but when
      # the variable name starts with `subject.`, the gear will utilize the value saved
      # in the Flywheel subject metadata.
      replace-with: "{{ subject.info.name }}"
```

The resulting profile for subject 003 given the above would be:

``` yaml
dicom:
  # date-increment can be any integer value since dicom.date-increment is defined in
  # example-csv-mapping.csv
  date-increment: -30
  remove_private_tags: true
  fields:
    - name: PatientBirthDate
      remove: true
    - name: PatientID
      replace-with: Patient_IDC
    - name: PatientName
      replace-with: Doe^John
```

The information in the subject_csv can also be used to update Flywheel metadata.  This
can be desirable to make sure that metadata fields, such as the `subject.label`, match
what is updated in the DICOM header.

The above example deid_profile can be extended to include a `flywheel` block:

```yaml
flywheel:
  subject:
    all: true
    fields:
      - name: label
        replace-with: "{{ SUBJECT_ID }}"
```

In this case, the `subject.label` in the destination project will be updated to match
the `SUBJECT_ID` specified in the subject_csv file.  So, for subject 003 in the example,
the `subject.label` in the destination project would be `Patient_IDC`.

### Config

* _project_path_
  * __Name__: project_path
  * __Type__: string
  * __Description__: The resolver path of the destination project
    (E.g., 'flywheel/test')

* _private_key_
  * __Name__: private_key
  * __Type__: string
  * __Description__: Asymmetric decryption: the resolver path and filename of the
    private key pem file, formatted as `<group>/<project>/files/<filename>` (E.g.,
    `flywheel/test/files/private_key.pem`) if the key is saved at the project level, or
    `<group>/<project>/<subject>/files/<filename>` if stored at the subject level, or
    `<group>/<project>/<subject>/<session>/<acquisition>/<filename>` if stored within
    an acquisition container.

* _public_key_
  * __Name__: public_key
  * __Type__: string
  * __Description__: Asymmetric encryption: the resolver path and filename(s) of the
    public key pem file(s), formatted as `<group>/<project>/files/<filename>`, with
    multiple key files separated by ', '
    (E.g. `flywheel/test/files/public_key1.pem, flywheel/test/files/public_key2.pem`)
    if the key is saved at the project level, or
    `<group>/<project>/<subject>/files/<filename>`
    if stored at the subject level, or
    `<group>/<project>/<subject>/<session>/<acquisition>/<filename>`
    if stored within an acquisition container.

* _secret_key_
  * __Name__: secret_key
  * __Type__: string
  * __Description__: Symmetric encryption: the resolver path and filename(s) of the
    secret key txt file(s), formatted as `<group>/<project>/files/<filename>` (E.g.
    `flywheel/test/files/secret_key.txt`) if the key is saved at the project level, or
    `<group>/<project>/<subject>/files/<filename>` if stored at the subject level, or
    `<group>/<project>/<subject>/<session>/<acquisition>/<filename>` if stored within
    an acquisition container.

* _overwrite_files_
  * __Name__: overwrite_files
  * __Type__: Enum[string] - [Skip, Cleanup, Cleanup_force, Replace]
  * __Default__: Skip
  * __Description__: Overwrite strategy when a file to be exported exists in
      destination containers. Options are Skip= Do not do anything if file exists at
      destination, Cleanup= Delete and upload a new file if file exists at destination
      (Error will be raised for smart copied project), Cleanup_force= Delete (force
      delete for smart copies project) if the file exists at destination and upload a
      new file, Replace= Upload a new version of the file if file exists at destination

* _debug_
  * __Name__: debug
  * __Type__: boolean
  * __Default__: false
  * __Description__: If true, the gear will print debug information to the log.

## Usage

1. User creates a destination project and enables gear rules. Importantly,
   a metadata indexing gear should be enabled to parse the
   de-identified DICOM headers. Further,
   the permissions on this project should be restricted until the user
   exporting the project has reviewed this project (after gear execution).
2. Within the source project, the
   [hierarchy-curator](https://gitlab.com/flywheel-io/scientific-solutions/gears/hierarchy-curator)
   can be used to modify container metadata in preparation for export (if needed).
3. User runs deid-export (this analysis gear) at the project, subject, or session
   level and provides the following:
    * Files:
        * A de-identification profile specifying how to
          de-identify/anonymize each file type
        * an optional csv that contains a column that maps to a
          Flywheel session or subject metadata field and columns that
          specify values with which to replace DICOM header tags
    * Configuration options:
        * The group_id/project name for the project to which to export
          anonymized files
        * Whether to overwrite files if they already exist in the target
          project

4. The gear will find/create all subject/session/acquisition containers
   associated with the destination analysis parent container.

5. The gear will attempt to download all files that match the `file-filter` list
   for any of the file profiles within the profile, de-identify them per the profile
   with matching file-filter, and upload them to the destination container

6. The gear will then write an output file reporting the status files
   that were exported. The gear will then exit.

### Encryption/Decryption

#### Encryption

To configure a gear run for encryption, there are a few requirements to ensure:

* Update the deid profile to specify encryption behaviors (see additional documentation
[here](https://flywheel-io.gitlab.io/public/migration-toolkit/pages/getting_started.html)
for composing a deid profile).
* Set key inputs in the deid profile to `"{{ PUBLIC_KEY }}"` or `"{{ SECRET_KEY }}`
as appropriate.
* Upload the key files (`.pem` for public key, `.txt` for secret key) to Flywheel.
* When configuring the gear, input the Flywheel path to the key file(s)
(i.e. `<group>/<project>/files/<filename>`).

``` yaml
dicom:
  # Setting asymmetric-encryption to true specifies how the EncryptedAttributesSequence
  # is encrypted
  asymmetric-encryption: true
  # Asymmetric encryption requires public-key input for encryption and private-key
  # for decryption
  public-key: "{{ PUBLIC_KEY }}"
  # Setting retain to true will store original values in the 
  # EncryptedAttributesSequence when values are changed/removed
  # If retain is not specified, only fields specified with the action "encrypt: true"
  # will be stored.
  retain: true
  fields:
    - name: PatientName
      replace-with: ANONYMIZED
    - name: PatientID
      # csv mapping is compatible with encrypt/decrypt
      replace-with: "{{ SUBJECT_ID }}"

flywheel:
  subject:
    all: false
    # For non-DICOM profiles, only symmetric encryption is available, and requires a
    # secret-key.
    secret-key: "{{ SECRET_KEY }}"
    fields:
      - name: label
        replace-with: "{{ SUBJECT_ID }}"
      - name: firstname
        # Setting encrypt: true encrypts the field in place
        encrypt: true
      - name: lastname
        encrypt: true
```

#### Decryption

Configuring a gear run for decryption is similar to encryption configuration:

* Update the deid profile to specify decryption behaviors (see additional documentation
[here](https://flywheel-io.gitlab.io/public/migration-toolkit/pages/getting_started.html)
for composing a deid profile).
* Set key inputs in the deid profile to `"{{ PRIVATE_KEY }}"` or `"{{ SECRET_KEY }}`
as appropriate.
* Upload the key files (`.pem` for private key, `.txt` for secret key) to Flywheel.
* When configuring the gear, input the Flywheel path to the key file(s)
(i.e. `<group>/<project>/files/<filename>`).

``` yaml
dicom:
  # Setting asymmetric-encryption to true specifies how the EncryptedAttributesSequence
  # was encrypted
  asymmetric-encryption: true
  # Asymmetric encryption requires public-key input for encryption and private-key for 
  # decryption
  private-key: "{{ PRIVATE_KEY }}"
  fields:
    - name: PatientName
      # The decrypt action restores the tag to the value stored in the
      # EncryptedAttributesSequence
      decrypt: true
    - name: PatientID
      decrypt: true

flywheel:
  subject:
    all: false
    # For non-DICOM profiles, only symmetric decryption is available,
    # and requires a secret-key.
    secret-key: "{{ SECRET_KEY }}"
    fields:
      - name: label
        # Because this field was replaced with csv mapping and not encryption, the
        # process should be reversed, swapping subject.label and SUBJECT_ID columns 
        # in the csv
        replace-with: "{{ SUBJECT_ID }}"
      - name: firstname
        # The decrypt action decrypts in-place
        decrypt: true
      - name: lastname
        decrypt: true
```

### Additional Considerations

* This gear cannot be run as gear rule. Gear rule are designed to run from an event that
  happens to a file and the deid-export gear is designed to run at the project, subject
  or session level.

* A notebook demonstrating how to batch run the gear at the subject level on the project
  is available in the Tutorial folder.

## Development

### Environment

This gear uses `poetry` as a virtual environment and dependency manager you can interact
with the gear using the following:

1. [Install poetry](https://python-poetry.org/docs/#installation)
2. Install dependencies (from within gear directory): `poetry install`
3. Enter virtual environment: `poetry shell`
