# Release Notes

## 1.7.0

__Enhancements__:

* Added capability to pull values from Flywheel subject metadata for subject-specific
templates

__Maintenance__:

* Split parser functions out from `run.py` into `fw_gear_deid_export/parser.py`
* Added testing for parser functions

## 1.6.1

__Maintenance__:

* Retry refactor
* Update Dockerfile and dependencies

__Fixes__:

* Fix bug where `subject.date_of_birth==""` causes the gear to fail

## 1.6.0

__Enhancements__:

* Update migration-toolkit reference
  * Allows for setting `datetime-min`, `datetime-max`, `jitter-min`,
  and `jitter-max`
  * Adds `encrypt` and `decrypt` actions
* Add config options `public_key`, `private_key` and `secret_key` to
pass in key file paths(s) for encryption/decryption

__Fixes__:

* On failed deid action, the gear skips export of the affected file,
reports in the csv, and job state is marked as fail

## 1.3.0

__Enhancement__:

* Add support to export file's zip_member_count and file tags
* Update CI-template reference and added pre-commit hooks
* Update migration toolkit reference to make deid UID dicom compliant

## 1.2.3

__Bug fix__:

* Use the correct capitalization for allowed container creation keywords.
