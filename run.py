#!/usr/bin/env python
import logging
import os

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_deid_export import container_export
from fw_gear_deid_export.parser import parse_args_from_context


log = logging.getLogger(__name__)


def main(gear_context):
    export_args = parse_args_from_context(gear_context)
    if not export_args:
        log.error("Exiting...")
        return 1
    else:
        error_count = container_export.export_container(**export_args)
        if os.path.isfile(export_args.get("csv_output_path")):
            log.info(
                f"Export completed with {error_count} errors "
                + f"log written {os.path.basename(export_args.get('csv_output_path'))}"
            )
            if error_count > 0:
                return 1
            else:
                return 0
        else:
            log.error("Export log failed to write")
            return 1


if __name__ == "__main__":
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        exit_status = main(gear_context)
    log.info(f"exit_status is {exit_status}")
    os._exit(exit_status)
