from pathlib import Path

import pytest

from fw_gear_deid_export.container_export import (
    load_template_dict,
    matches_file,
)
from fw_gear_deid_export.deid_template import load_deid_profile

DATA_ROOT = Path(__file__).parent / "data"


def test_can_load_template_file(data_file):
    template_path = data_file("example1-deid-profile.yaml")
    res = load_template_dict(str(template_path))
    assert res is not None
    assert "dicom" in res
    assert "flywheel" in res
    assert res["dicom"]["fields"][-1]["replace-with"] == "YES"


def test_load_template_file_raises_with_path_issue():
    with pytest.raises(FileNotFoundError):
        load_template_dict(str(DATA_ROOT / "does-not-exist.yaml"))


def test_matches_file(data_file):
    template_path = data_file("example-3-deid-profile.yaml")
    template_dict = load_template_dict(str(template_path))
    template_dict["dicom"]["file-filter"] = []
    deid_profile, _ = load_deid_profile(template_dict)
    assert matches_file(deid_profile, {"type": "dicom"})
    assert matches_file(deid_profile, {"name": "test.jpg"})
    template_dict["jpg"]["file-filter"] = []
    deid_profile, _ = load_deid_profile(template_dict)
    assert not matches_file(deid_profile, {"name": "test.jpg"})
    template_dict.pop("dicom")
    deid_profile, _ = load_deid_profile(template_dict)
    assert matches_file(deid_profile, {"type": "dicom", "name": "test.dcm"})
