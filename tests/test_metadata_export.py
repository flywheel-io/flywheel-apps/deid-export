import io
import json
from contextlib import nullcontext as does_not_raise
from datetime import datetime
from unittest.mock import MagicMock

import pytest
from flywheel_migration.deidentify.json_file_profile import JSONRecord

from fw_gear_deid_export.metadata_export import (
    META_ALLOW_LIST_DICT,
    ContainerToJSONFormatter,
    JSONToContainerFormatter,
    get_deid_fw_container_metadata,
)


@pytest.mark.parametrize(
    "c_type,to_dump,raises",
    [
        ("subject", {"cohort": "Control"}, does_not_raise()),
        ("subject", {"cohort": "control"}, pytest.raises(ValueError)),
        ("subject", {"sex": "male"}, does_not_raise()),
    ],
)
def test_json_to_container_format(c_type, to_dump, raises):
    formatter = JSONToContainerFormatter(container_type=c_type)
    json_file = io.TextIOWrapper(io.BytesIO())
    json_file.write(json.dumps(to_dump))
    json_file.seek(0)
    record = JSONRecord(json_file)
    with raises:
        out = formatter.format(record)
        assert out == to_dump


def test_formatter_filter_container(fw_project):
    project = fw_project()
    f_project = ContainerToJSONFormatter.filter(project)
    assert not all(
        [k in META_ALLOW_LIST_DICT["project"] for k in project.to_dict().keys()]
    )
    assert all([k in META_ALLOW_LIST_DICT["project"] for k in f_project.keys()])


def test_get_deid_fw_container_metadata(fw_project):
    project = fw_project()
    client = MagicMock()
    subject = project.subjects()[0]
    session = project.subjects()[0].sessions()[0]
    session.id = "session_id"
    session.timestamp = datetime(2020, 8, 17, 14, 15, 00)
    client.get_subject = MagicMock(return_value=subject)
    client.get_project = MagicMock(return_value=project)
    config = {
        "all": False,
        "fields": [
            {
                "name": "label",
                "regex-sub": [
                    {
                        "input-regex": "(?P<var>.*)",
                        "output": "{subject.label}_{var}",
                        "groups": [
                            {"name": "var", "replace-with": "TEST"},
                            {"name": "subject.label", "identity": True},
                        ],
                    }
                ],
            }
        ],
    }
    record = get_deid_fw_container_metadata(client, config, session)
    assert record["label"] == "sub-0_TEST"
    assert len(record) == 2
    assert record["info"]["export"]["origin_id"] is not None

    config["all"] = True
    record = get_deid_fw_container_metadata(client, config, session)
    assert record["label"] == "sub-0_TEST"
    assert isinstance(record["timestamp"], datetime)
    # SHould remove all None values
    assert all([True if k is not None else False for k in record.values()])
    assert len(record) <= len(META_ALLOW_LIST_DICT["session"])


@pytest.mark.parametrize(
    "given_val, expect_none",
    [
        # Parsed as unix timestamps by SDK call
        # Format coerces into string
        ("0", False),
        (0, False),
        (0.0, False),
        # Datetimes become strings during ContainertoJSON
        (datetime(1970, 1, 1), False),
        ("", True),
        (None, True),
    ],
)
def test_get_deid_fw_container_metadata_subject_dob(given_val, expect_none, fw_project):
    project = fw_project()
    client = MagicMock()
    subject = project.subjects()[0]
    client.get_subject = MagicMock(return_value=subject)
    client.get_project = MagicMock(return_value=project)
    config = {"all": True}

    subject.date_of_birth = given_val
    record = get_deid_fw_container_metadata(client, config, subject)
    if expect_none:
        # Value should be None, key should have been removed.
        assert "date_of_birth" not in record
    else:
        assert record["date_of_birth"] == str(given_val)
