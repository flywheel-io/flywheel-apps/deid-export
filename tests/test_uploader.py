"""Test Uploader"""

import io
from unittest import mock

import pytest
from flywheel import AcquisitionOutput

from fw_gear_deid_export.uploader import (
    TICKETED_UPLOAD_PATH,
    Uploader,
    get_upload_ticket_suggested_headers,
    pluralize,
)


@pytest.fixture
def uploader():
    client = mock.MagicMock()
    uploader = Uploader(client)
    return uploader


def test_supports_signed_url_should_return_true_if_feature_true(uploader):
    uploader.fw_client.get_config.return_value = {
        "features": {"signed_url": True},
        "signed_url": False,
    }

    result = uploader.supports_signed_url()

    assert result is True


def test_supports_signed_url_should_return_true_if_root_config_true(uploader):
    uploader.fw_client.get_config.return_value = {
        "features": {"signed_url": False},
        "signed_url": True,
    }

    result = uploader.supports_signed_url()

    assert result is True


def test_supports_signed_url_should_return_false_if_feature_and_root_config_false(
    uploader,
):
    uploader.fw_client.get_config.return_value = {
        "features": {"signed_url": False},
        "signed_url": False,
    }

    result = uploader.supports_signed_url()

    assert result is False


def test_upload_should_call_upload_file_to_container_if_supports_signed_url_false(
    uploader,
):
    uploader._supports_signed_url = False
    acquisition = mock.MagicMock(
        spec=AcquisitionOutput,
        container_type="acquisition",
        files=[],
    )
    file_object = io.StringIO("Test data")

    uploader.upload(
        acquisition, "file.dcm", file_object, upload_session=mock.MagicMock()
    )

    uploader.fw_client.upload_file_to_acquisition.assert_called_once()


def test_upload_should_return_none_if_upload_function_doesnt_exist(
    uploader,
):
    group = mock.MagicMock(container_type="group")
    file_object = io.StringIO("Test data")
    del uploader.fw_client.upload_file_to_group

    result = uploader.upload(
        group, "file.dcm", file_object, upload_session=mock.MagicMock()
    )

    assert result is None


def test_signed_url_upload_retries_if_500(uploader):
    upload_session = mock.MagicMock()
    upload_session.put.return_value.status_code = 503
    acquisition = mock.MagicMock(container_type="acquisition")
    file_object = io.StringIO("Test data")

    with mock.patch("fw_gear_deid_export.uploader.time.sleep"):
        uploader.signed_url_upload(
            acquisition, "file.dcm", file_object, upload_session=upload_session
        )

    assert upload_session.put.call_count == 4  # max retries + initial call


@pytest.mark.parametrize(
    "status_code, call_count", [(500, 4), (429, 4), (401, 1), (200, 1)]
)
def test_signed_url_upload_retries(uploader, status_code, call_count):
    upload_session = mock.MagicMock()
    upload_session.put.return_value.status_code = status_code
    acquisition = mock.MagicMock(container_type="acquisition")
    file_object = io.StringIO("Test data")

    with mock.patch("fw_gear_deid_export.uploader.time.sleep"):
        uploader.signed_url_upload(
            acquisition, "file.dcm", file_object, upload_session=upload_session
        )

    assert upload_session.put.call_count == call_count


def test_pluralize():
    assert pluralize("analysis") == "analyses"
    for item in ["subject", "session", "acquisition", "file", "project"]:
        assert pluralize(item) == item + "s"


def test_get_upload_ticket_suggested_headers():
    assert get_upload_ticket_suggested_headers(None) is None
    assert get_upload_ticket_suggested_headers({}) is None
    headers_dict = {"spam": "eggs"}
    assert (
        get_upload_ticket_suggested_headers({"headers": headers_dict}) == headers_dict
    )


@pytest.mark.parametrize(
    "method, query_params",
    [
        ("POST", [("ticket", "")]),
        ("POST", [("ticket", "1234567890")]),
        ("POST", [(None, None)]),
        ("GET", []),
    ],
)
def test_call_api(uploader, method, query_params):
    uploader.call_api(TICKETED_UPLOAD_PATH, method, query_params=query_params)

    uploader.fw_client.api_client.call_api.assert_called_once()
