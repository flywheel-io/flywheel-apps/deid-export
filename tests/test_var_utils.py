from pathlib import Path
from unittest.mock import MagicMock

import pandas as pd
import pytest

from fw_gear_deid_export.var_utils import (
    create_jinja_var_df,
    get_csv_df,
    get_jinja_variables,
    get_subject_df,
)

DATA_ROOT = Path(__file__).parent / "data"


@pytest.fixture
def mock_get_jinja_vars(mocker):
    vars = mocker.patch("fw_gear_deid_export.var_utils.get_jinja_variables")
    return vars


@pytest.fixture
def mock_get_subject_df(mocker):
    df = mocker.patch("fw_gear_deid_export.var_utils.get_subject_df")
    return df


@pytest.fixture
def mock_get_csv_df(mocker):
    df = mocker.patch("fw_gear_deid_export.var_utils.get_csv_df")
    return df


def test_get_jinja_variables():
    profile_path = DATA_ROOT / "example4-deid-profile-jinja.yaml"

    expected_subj_vars = ["subject.info.cool.cats"]
    expected_csv_vars = ["SUBJECT_ID", "PATIENT_BD_BOOL", "DATE_INCREMENT"]

    subj_vars, csv_vars = get_jinja_variables(profile_path)
    assert sorted(subj_vars) == sorted(expected_subj_vars)
    assert sorted(csv_vars) == sorted(expected_csv_vars)


def test_get_subject_df(fw_project, caplog):
    project = fw_project()
    subject = project.subjects()[0]
    subject.firstname = "Bob"
    subject.info = {"something": "test value", "skip this": "value"}

    subj_vars = [
        "subject.firstname",
        "subject.info.something",
        "subject.info.does.not.exist",
    ]

    res = get_subject_df(subject, subj_vars)

    assert res.loc[0, "subject.label"] == "sub-0"
    assert res.loc[0, "subject_firstname"] == "Bob"
    assert res.loc[0, "subject_info_something"] == "test value"
    assert res.loc[0, "subject_info_does_not_exist"] == ""
    assert "subject_info_skip this" not in res.keys()
    assert "No value for sub-0 subject.info.does.not.exist" in caplog.text


def test_get_csv_df():
    profile_path = DATA_ROOT / "example4-deid-profile-jinja.yaml"
    csv_path = DATA_ROOT / "example-csv-mapping.csv"
    csv_vars = ["SUBJECT_ID", "PATIENT_BD_BOOL", "DATE_INCREMENT"]

    res = get_csv_df(profile_path, csv_path, csv_vars=csv_vars)

    # Spot checking
    assert res.loc[0, "subject.label"] == "001"
    assert res.loc[0, "DATE_INCREMENT"] == "-15"
    assert res.loc[1, "PATIENT_BD_BOOL"] == "TRUE"
    assert res.loc[2, "SUBJECT_ID"] == "IDC"


def test_get_csv_df_raises_if_missing_required_columns():
    profile_path = DATA_ROOT / "example4-deid-profile-jinja.yaml"
    csv_path = DATA_ROOT / "example-csv-mapping.csv"
    csv_vars = ["SUBJECT_ID", "PATIENT_BD_BOOL", "DATE_INCREMENT", "DOESNT_EXIST"]

    with pytest.raises(
        ValueError, match="Column DOESNT_EXIST is missing from dataframe"
    ):
        get_csv_df(profile_path, csv_path, csv_vars=csv_vars)


# This test runs correctly when run individually, but when running the test set
# maintains the "Column DOESNT_EXIST..." ValueError raised in the previous test.
@pytest.mark.skip(reason="Odd caching issue")
def test_get_csv_df_raises_if_subj_col_not_unique():
    profile_path = DATA_ROOT / "example4-deid-profile-jinja.yaml"
    csv_path = DATA_ROOT / "example-csv-mapping-not-unique.csv"
    csv_vars = ["SUBJECT_ID", "PATIENT_BD_BOOL", "DATE_INCREMENT"]

    with pytest.raises(ValueError, match="subject.label is not unique in csv"):
        get_csv_df(profile_path, csv_path, csv_vars=csv_vars)


def test_create_jinja_var_df_no_vars(mock_get_jinja_vars):
    profile_path = DATA_ROOT / "example4-deid-profile-jinja.yaml"
    origin = MagicMock()
    csv_path = DATA_ROOT / "example-csv-mapping.csv"
    mock_get_jinja_vars.return_value = None, None

    res = create_jinja_var_df(profile_path, origin, csv_path)
    assert not res


def test_create_jinja_var_merge(
    mock_get_jinja_vars, mock_get_subject_df, mock_get_csv_df
):
    profile_path = DATA_ROOT / "example4-deid-profile-jinja.yaml"
    origin = MagicMock()
    csv_path = DATA_ROOT / "example-csv-mapping.csv"
    mock_get_jinja_vars.return_value = (
        ["subject.firstname", "subject.info.something"],
        ["SUBJECT_ID", "PATIENT_BD_BOOL", "DATE_INCREMENT"],
    )
    mock_get_subject_df.return_value = pd.DataFrame(
        {
            "subject.label": ["001"],
            "subject_firstname": ["Bob"],
            "subject_info_something": ["test"],
        }
    )
    mock_get_csv_df.return_value = pd.DataFrame(
        {
            "subject.label": ["001"],
            "SUBJECT_ID": ["IDA"],
            "PATIENT_BD_BOOL": ["FALSE"],
            "DATE_INCREMENT": ["-15"],
        }
    )

    res = create_jinja_var_df(profile_path, origin, csv_path)
    # Spot checking
    assert res.loc[0, "subject.label"] == "001"
    assert res.loc[0, "subject_firstname"] == "Bob"
    assert res.loc[0, "DATE_INCREMENT"] == "-15"


def test_create_jinja_var_merge_no_csv(mock_get_jinja_vars, caplog):
    profile_path = DATA_ROOT / "example4-deid-profile-jinja.yaml"
    origin = MagicMock()
    csv_path = None
    mock_get_jinja_vars.return_value = (
        None,
        ["SUBJECT_ID", "PATIENT_BD_BOOL", "DATE_INCREMENT"],
    )

    res = create_jinja_var_df(profile_path, origin, csv_path)
    assert not res
    assert "no subject_csv was provided" in caplog.text
